# Cover Letter

Dear Gitlab,

I wasn't sure how to start this document. Then I realized that there are not many companies out there for which I would love to work more than for Gitlab, and I've considered it a good sentence to start with. Below I try to explain why it's aligned with my values:

## Gitlab - the tool I use everyday
Before I start any implementation, research, or cooperation with other developers, I create a new project in Gitlab. I am convinced that any healthy environment must have code reviews, automized CI/CD pipelines and full transparency, that is something Gitlab offers as the core elements. And as I have noticed in the Gitlab Direction, the same values and principles will be offered to designers, POs, and other stakeholders who contribute in today's software development. That is great because I believe that the software development industry is unique than other industries because of the transparency and sharing context between disciplines. I would love to be part of the direction which extends the same attitude and mindset to a broader audience. I am convinced that if more people have the same mindset, it will make the world a better place.

I feel that working at Gitlab would be an excellent fit for what I consider essential. My main long-term interest is the automatization of CI/CD, linting, npm audits, unit tests, visual regression testing, e2e tests. The Gitlab Pipelines is the tool which makes all of that possible. Moreover, I like to work with Vue.js and Nuxt as FE frameworks. And it was actually one of the Gitlab employees, Filipa Lacerda, whose talk I saw at the very first Vue conference in the world in Wrocław in 2017. There I realized how versatile the Vue.js is and how it can be the way to modernize a vast code base where completely rewriting is not an option.

Also, it is great for the motivation that Gitlab has the GitLab Direction, which is known by all the contributors who can see their commitment to the goal.

## Remote Work
I've spent most of my career working remotely. At my current company, I was the first person who promoted the remote work, so I spent half of a year abroad in Portugal, where I experienced the fully remote working. During that time, my colleagues and I were evaluating the impacts on productivity, and we were writing internal guidelines for other employees. The experience was really amazing, and I consider that period as the most productive I have ever had. Even though the conclusions were positive, we weren't able to fully implement remote work aspects to our company culture. In our agency environment projects and teams are constantly changing, so every time before the remote cooperation settles down, the project is usually over. I am continually trying to increase awareness; however, it's hard to change the whole company culture.

So, it would be my dream to work at Gitlab where all of the people have a remote mindset. I believe that it leads to excellent working conditions and better results.

## Product
I observe that working on a product suits me much more than working on short term projects. One of my core values is that I do care about what I am working on. I think about it when I am sitting in a tram, sometimes I wake up, and I have an excellent idea of how to tackle a problem I'm facing. Overall it is vital for me to deliver a good result.

Based on what I wrote above, I would be more than happy to work at Gitlab, and I am convinced that I would be a beneficial member of the Gitlab.

Best Regards,
Oldrich
