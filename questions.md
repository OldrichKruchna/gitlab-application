# Questions

## The questions below are purposefully open-ended. Please take your time completing the questions, and when you are done, please submit your application: Tell us about your latest experience working with modern frameworks/libraries, what were the advantages? Disadvantages? How much experience do you have with said framework/library?
I feel the most confident with Vue.js, which gives me a feeling that I can focus more on the important things like features, code quality, unit testing, accessibility, reusability and rather than bending the framework or thinking how to do it. My experience is that whatever I needed to achieve with Vue.js, it was possible it the past and that makes me confident whenever I need to do important technological or architecture decision.

Between advantages I would definitely count great documentation, that makes the framework highly accessible by new developers. Also it's very lightweight and vercetily. Tt doesn't force you to specific way of work. I guess for that reason it's very popular among different groups of developers.

I can be easily used to render FE components where the interactivity is needed, when the whole webpage can server side rendered by completely different language and framework.

I also do like the single file component structure, the community around the framework, wonderful Nuxt framework which I financially support every month.

Between disadvantages I would count guite huge size of the smallest possible bundle 34 KB gzipped which for SPA application is quite reasonable, but for one simple component in whole webpage not so much. But according the Evan You, as he announced at the Vuejs London 2018 in his talk "Roadmap for the next steps of Vue" where he was speaking about Vue.js 3 [https://docs.google.com/presentation/d/1yhPGyhQrJcpJI2ZFvBme3pGKaGNiLi709c37svivv0o/edit#slide=id.g42acc26207_0_169] that the new version will allow just parts, directives you need and then could push the minimum size to 10 KB.

The other disadvantage could be the virtual dom it self, I saw the amazing talk of Rich Harris from Svelte who suggest a different way how to bring reactivity to the javascript, where at the build time they build a topological order of variables which defines what variable depends on what, and when any change happens they don't have to do comparison of virtual dom at all because they how what they need to update based on the built topological order. So maybe there are better ways how to tackle reactivity.


## Please navigate to the page: https://gitlab.com/sytses. From a frontend technical perspective: what is the browser doing to display this page? * 
1. The browser fetches the HTML document
2. It starts to parse the document and composing DOM
It finds 5 script references, however all of them have `defer` so no need to wait for them with the DOM composition.
3. Once the DOM is done the styles are fetched and the browser starts to compose 
3. All the scripts are deferred so now need to wait the
First of all the index.html is fetched.
The the parsing of the document starts and the browser start to compose the DOM.
If it finds any script it needs to fetch, parse and execute the javascript file first.
After the parsing is done, the DOM is composed.

Then the styles are loaded and to each node the browsers added correct styles.
Then the layout tree is composed, which contains just elements which needs to be displayed. So if some element has display: none it's not there.

Then the layers are composed based on which elements needs to be updated often and which not. That can be defined by developer with will-change attribute

And those layers are compose together and rendered/rastarized by the Compositor.

## Tell us about your latest "hard to debug" frontend problem. How did you resolve it? Which tools did you use? * 
The latest "hard to debug" frontend problem was probably that I needed to develop a web app inside of a webview of native app. The app offered an interface which provided information about logged-in  user, state of the app, it was possible to set a title, etc. All of those information were available via events which were triggered on global window object. And I had basic documentation about how it works, but it was not really possible to develop or test all the scenarios within the app. So I built a simple debug tool with simple UI, in which by clicking I could trigger those events with some dummy data. And equipped by this was I was able to simulate all possible states needed for development.


## Tell us about the most advanced/exciting/mind-blowing frontend thing you have built. * 
I've built so called feature app for a website called https://wohnglueck.de/ The page itself contains mostly content with simple elements which doesn't have much interactivity, but we had a use case that for specific task on the website, filtering of prefabric houses, we needed  filtering and sortign where more interactivity was needed, yet still we need everything SEO optimased. So I chosen the Nuxt, because I was the most confident with the Vue itself and Nuxt offered everything what we needed basically out of the box.

Only unkhown was integration of the result to micro-frontend architecture, but turned out that Nuxt was capable of everything. So we rendered just the feature app without any header, meta data, menu, those are elements which are provided by the integrator/parent website. And nuxt can do it.

Everything is deployed to AWS Lambda.
I am really proud of this project, because we built most of the stack, architecture and infrastructure in the first two weeks and then we were working on the new features.
But from the very first day we had Gitlab pipeline with linting, npm audits, unit tests and deployement. And that made a lot of things much more easier during the development.

