# CV

## Personal information
- Name: Oldřich Kruchňa
- Date of birth: 8.11.1990
- Nationality: Czech
- Address: Starobylá 498, 14900, Prague
- Phone number: +420739178572
- Email: kruchna.o@gmail.com

## Education
- 2006 - 2010 - Gymnázium Oty Pavla
- 2010 - 2013 - Bachelor in Computer Science at CTU in Prague with specialization on web and computer graphics

## Work Experience

### Senior Product Developer at SinnerSchrader

At SinnerSchrader my main responsibilities were:

- to kickoff new frontend projects
- to mentor other developers 
- to stabilize projects
- to bring standards, unit tests, pipelines and workflows to legacy projects
- to promote remote work

What I am proud of:
- built server side rendering architecture
- integrated it as micro-frontend service to parent website
- built building/deployment Gitlab pipeline from the first days
- introduced code standards
- increased awareness of unit tests and TDD
- created maturity matrices for Continuous Delivery and Remote Work

I worked with:
Vue, Vuex, Nuxt, Jest, Webpack, code splitting, Eslint, Express, GraphQL, AWS, Lambda, Serverless, Gitlab Pipelines, Cypress, BEM, Sass, Less

### Javascript Developer at D21

At D21 my main responsibilities were:

- to finish project on time, because I joined to support for the last 6 months
- to be able to quickly react on ongoing president elections
- to deploy as fast as possible to production

What I am proud of:
- the project was successful ;)
- we managed to finish everything on time

I worked with:
Angular, Redux, RxJS, Typescript, Webpack, Express, GraphQL, AWS, Lambda, Less

### Frontend Developer at CRM Factory

At CRM Factory my main responsibilities were:

- to build web applications for automotive services in China - MySales and MyService
- to build highly configurable modules
- to prepare modules for white labelling for different clients - Ford, VW and Audi.
- to write versioned and maintainable CSS and Javascript modules

What I am proud of:
- I learned so much
- really good remote communication
- working with really advanced versioning modules which were used for different applications

I worked with:
Angular 1.x, PhoneGap, Cordova, BEM, Sass

### Frontend Developer at Improvisio

At Improvisio my main responsibilities were:

- to OptimTyre web-based mobile application
- later on to maintain the OptimTyre

What I am proud of:
- I learned so much
- the OptimTyre was one the biggest projects I've ever worked with

I worked with:
Angular 1.x, PhoneGap, Cordova, BEM, Sass
